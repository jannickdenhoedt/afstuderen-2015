@extends('app')

@section('content')
		<div class="col-md-6 friends-container" ng-app="socialPlatformApp" ng-cloak ng-controller="friendsController">
		<div id="friends" class="panel panel-default">
			<div class="panel-heading">
				<h2>Vriendenboek</h2>
			</div>
				<div class="panel-body">
					<div class="friends-overview-loading" ng-if="loading == true">
						<img src="{{ asset('/img/350.GIF') }}">
					</div>
					<div class="row">
						<div class="post-overview col-md-6" ng-repeat='friend in friends'>
							<div class="thumbnail">      
			                    <div class="caption clearfix">
			                    	<div class="user-desc">
			                    	<a ng-click="stopFriendsTour()" href="{{ url('profile/user') }}/<% friend.id %>">
				                    	<span class="friend-image" style="background : url({{ asset('/uploads/') }}/user_<%friend.id%>/<%friend.filename%>) top center; background-size:cover;"></span>
				                        	<p><% friend.name %></p>
			                        </a>
			                        </div>
			                        <a href="{{route('chat')}}">
			                        	<span class="btn btn-default btn-chat">Gesprek</span>
			                        </a>
			                    </div>
				            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>document.body.className = 'friends';</script>
@endsection
