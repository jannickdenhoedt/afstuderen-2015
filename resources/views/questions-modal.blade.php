<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Vragen om te stellen</h4>
      </div>
      <div class="modal-body">
        <ul class="list-group">
          <li class="list-group-item"><a href="#">Hoe gaat het met jou?</a></li>
          <li class="list-group-item"><a href="#">Wat heb je vandaag gedaan?</a></li>
        	<li class="list-group-item"><a href="#">Waar woon je?</a></li>
        	<li class="list-group-item"><a href="#">Wat zijn je hobby's?</a></li>
        	<li class="list-group-item"><a href="#">Zit je op school of werk je?</a></li>
        	<li class="list-group-item"><a href="#">Van welke muziek jou je?</a></li>
        	<li class="list-group-item"><a href="#">Wat is jouw lievelingseten?</a></li>
        	<li class="list-group-item"><a href="#">Heb je broers of zussen?</a></li>
        	<li class="list-group-item"><a href="#">Heb je een relatie?</a></li>
        	<li class="list-group-item"><a href="#">Wat is jouw lievelingsland voor vakantie?</a></li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
      </div>
    </div>
  </div>
</div>