@extends('app')

@section('content')
		<div class="col-md-6 find-friends-container" ng-app="socialPlatformApp" ng-cloak ng-controller="findFriendsController">
		<div id="friendInvites" class="panel panel-default">
			<div class="panel-heading">
				<h2>Uitnodigingen</h2>
				</div>
				<div class="panel-body">
					<div class="find-friends-overview-loading" ng-if="loading == true">
						<img src="{{ asset('/img/350.GIF') }}">
					</div>
					<div class="row">
					<div ng-show="!invitations.length" class="col-md-12" >
						Je hebt nog geen uitnodigingen. 
					</div>
						<div class="accept-friends-overview col-md-12" ng-repeat='invitation in invitations'>
							<div class="thumbnail">      
			                    <div class="caption clearfix">
			                    	<div class="user-desc">
				                    	<a href="{{ url('profile/user') }}/<% invitation.id %>">
					                    	<span class="new-friend-image" style="background : url({{ asset('/uploads/') }}/user_<%invitation.id%>/<%invitation.filename%>) top center; background-size:cover;"></span>
					                        	<p><% invitation.name %></p>
				                        </a>
			                        </div>
		                        		<div class="btn-success btn friend-accept" ng-click="acceptFriend(invitation.id)"><span class="btn-text">Accepteren</span><img src="{{ asset('/img/icons/iconmonstr-user-11-icon-256.png') }}"></span></div>
			                    </div>
				            </div>
						</div>
					</div>
				</div>
			</div>

		<div id="findFriends" class="panel panel-default">
			<div class="panel-heading">
				<h2>Vrienden vinden</h2>
				</div>
				<div class="panel-body">
					<div class="find-friends-overview-loading" ng-if="loading == true">
						<img src="{{ asset('/img/350.GIF') }}">
					</div>
					<div class="row">
					<div class="col-md-12">
						<div class="alert friend-invite-succes alert-succes" role="alert">
							<ul class="succes">Vriend is uitgenodigd!</ul>
						</div>
					</div>
					<div ng-show="!friends.length" class="col-md-12">Geen Vrienden om te vinden</div>
						<div class="find-friends-overview col-md-12" ng-repeat='friend in friends'>
							<div class="thumbnail">      
			                    <div class="caption clearfix">
			                    	<div class="user-desc">
				                    	<a href="{{ url('profile/user') }}/<% friend.id %>">
					                    	<span class="new-friend-image" style="background : url({{ asset('/uploads/') }}/user_<%friend.id%>/<%friend.filename%>) top center; background-size:cover;"></span>
					                        	<p><% friend.name %></p>
				                        </a>
			                        </div>
		                        		<div class="btn-primary btn friend-invitation" ng-click="inviteFriend(friend.id)"><span class="btn-text">Vriend uitnodigen</span><img src="{{ asset('/img/icons/iconmonstr-user-11-icon-256.png') }}" /></div>
			                    </di v>
				            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>document.body.className = 'find-friends';</script>
@endsection
