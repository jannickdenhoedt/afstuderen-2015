<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Laravel</title>

		<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('/css/screen.css') }}" rel="stylesheet">

		<!-- Fonts -->
		<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	</head>

	<body>

		<div class="home-wrapper">
			<div class="container home-block">
			<div class="col-md-6 col-md-offset-3">
				<h1>Welkom bij social platform</h1>
			</div>
				<div class="col-md-4 col-md-offset-4">
					<a href="{{ url('/auth/register') }}"><button class="btn btn-default">Registreer</button></a>
					<a href="{{ url('/auth/login') }}"><button class="btn btn-default">Login</button></a>
				</div>

			</div>
		</div>
			
		
		
	</body>
</html>
