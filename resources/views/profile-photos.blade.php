@extends('app')

@section('content')
	@foreach($profileInfo as $info)
		<div class="col-md-6 photo-container" ng-app="socialPlatformApp" ng-cloak ng-controller="getPhotosController" ng-init="init({{$profileId}})">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Foto's van {{$info->name}}</h2>
				</div>
				<div class="panel-body">
					<div class="photo-overview-loading" ng-if="loading == true">
						<img src="{{ asset('/img/350.GIF') }}">
					</div>
					<div class="row">
						<div class="photo-overview col-xs-6" ng-repeat='photo in photos'>
							<div class="thumbnail">      
			                    <a href="#" data-featherlight="{{ asset('/uploads/') }}/user_<% photo.user_id+'/'+photo.filename %>">
	                        		<img ng-src="{{ asset('/uploads/') }}/user_<% photo.user_id+'/'+photo.filename %>">
	                        	</a>
				            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		<script>
			document.body.className = 'photos';
		</script>
@endsection
