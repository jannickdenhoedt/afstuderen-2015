@extends('app')

@section('content')
<div ng-app="socialPlatformApp" ng-cloak ng-controller="chatController">
  <div class="col-md-6 chat-container">
    <div id="friends" class="panel panel-default">
    <div class="loading" ng-if="loading == true">
        <img src="{{ asset('/img/350.GIF') }}">
      </div>
      <div class="panel-heading">
        <h2>Gesprek met: <% receiver.name %></h2>
      </div>
        <div class="panel-body">
          <div class="empty" ng-if="empty == true">
            <h2>Klik rechts op een vriend om mee te praten</h2>
            <a href="{{route('findFriends')}}">
              <h3>Heb je nog geen vrienden klik hier om vrienden te maken</h3>
            </a>
          </div>
        <div class="row">
            <div class="col-md-12">
              <div id="messages" class="messages-overview" >
                <div ng-repeat='message in messages' class="clearfix row">
                    <div ng-if="message.senderId == {{$profileId}}" class="message message-sent clearfix col-md-12">
                          <span class="chat-image" style="background : url({{ asset('/uploads/') }}/user_<%profile.id%>/<%profile.filename%>) top center; background-size:cover;"></span>
                          <a href="{{ url('profile/user') }}/<% profile.id %>">
                            <span class="profile-name"><% profile.name %></span>
                          </a>
                          <div class="message-text"><p><% message.message %></p></div>    
                    </div>
                    <div ng-if="message.senderId != {{$profileId}}" class="message message-received clearfix col-md-12">
                          <span class="chat-image" style="background : url({{ asset('/uploads/') }}/user_<%receiver.id%>/<%receiver.filename%>) top center; background-size:cover;"></span>
                          <a href="{{ url('profile/user') }}/<% receiver.id %>">
                            <span class="profile-name"><% receiver.name %></span>
                          </a>
                          <div class="message-text"><p><% message.message %></p></div>
                    </div>
                </div>  
              </div>
                <div class="isTyping"></div>
                <div id="sent-form">
                  <form action="">
                    <div class="row">
                      <div class="col-md-10">
                        <input id="m" autocomplete="off" class="form-control" />
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-success">Verstuur</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-toggle="modal" data-target="#myModal" class="questions btn btn-primary">Kies een vraag om te stellen</div>
    </div>
  <div class="col-md-3 chats-wrapper">
    <h1>Gesprekken</h1>
    <div class="chat-friends friend_<% friend.id%>" ng-repeat='friend in friends'>      
      <a ng-click="getChat(friend.id)" href="#sent-form" class="clearfix">
        <span class="friend-image" style="background : url({{ asset('/uploads/') }}/user_<%friend.id%>/<%friend.filename%>) top center; background-size:cover;"></span>
        <p><% friend.name %></p> 
      </a>
    </div> 
  </div>
<!-- modal -->
@include('questions-modal')
</div>

<!-- background color -->
<script>
document.body.className = 'chat';
</script>

@endsection
