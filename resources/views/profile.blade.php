@extends('app')
@section('content')

@foreach($profileInfo as $info)
<div class="col-md-6 friends-container post-overview-container" ng-app="socialPlatformApp" ng-cloak ng-controller="profileController">
	<div class="row">
		<div class="panel panel-default profile-info">
			<div class="panel-heading clearfix">
			<div class="row">
					<div class="profile-image-wrapper col-md-4">
						<!-- profile image using ng-init background -->
						<div class="col-md-12">
							<div class="profile-image-thumb">
								<a href="#" data-featherlight="{{ asset('/uploads/user_'.$profileId.'/'.$info->filename) }}">
									<span id="profile-image" class="profile-image" ng-init="profileImage = '/{{$info->filename}}'"  
										  ng-style="{'background' : 'url({{ asset('/uploads/user_'.$profileId.'/') }}' + profileImage + ') top center', 'background-size' : 'cover'}" >
										<span ng-if="profileImageLoading == true">
											<span class="profile-image-loading" ><img src="{{ asset('/img/350.GIF') }}">
											</span>
										</span>
									</span>
								</a>
							</div>
							@if(Auth::user()->id == $profileId)
							<form id="profile-image-upload" action="{{ route('addProfileImage', []) }}" method="post" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="profileId" value="{{ $profileId }}">

									<div class="form-group">
										<div id="file-upload-btn" class="fileUpload btn btn-primary">
										    <span><span class="btn-text">Foto bewerken</span><img src="{{ asset('/img/icons/iconmonstr-photo-camera-icon.png') }}"></span>
										    <input id="uploadBtn" type="file" class="upload" name="filefield">
										</div>	
										<span id="file-name"></span>
									</div>	   
							</form>
							@endif
						</div>
					</div>
					<div class="col-md-8">
						<div class="profile-name col-md-12">
							<h1>{{$info->name}}</h1>
						</div>
						@if(Auth::user()->id != $profileId)
						<!-- <div class="profile-menu col-md-12" data-example-id="simple-nav-stacked">
						    <ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
						      <li role="presentation" class="menu-item clearfix ">
						      	<a href="#">
							      	<span class="icon icon-new-friends" aria-hidden="true">
							      		<img src="{{ asset('/img/icons/iconmonstr-user-11-icon-256.png') }}">
							      	</span> 
							      	<span class="menu-text">Vriend uitnodigen</span>
						      	</a>
						      </li>
						      <li role="presentation" class="menu-item clearfix">
							      <a href="#">
								      <span class="icon icon-messages" aria-hidden="true">
								      	<img src="{{ asset('/img/icons/iconmonstr-speech-bubble-15-icon-256.png') }}">
								      </span> 
								      <span class="menu-text">Gesprek starten</span>
							      </a>
						      </li>
						    </ul>
					  	</div> -->
					  	@endif
				  	</div>
			  	</div>
			</div>
			<div id="info-body" class="panel-body">
				<div class="info-header">
					<h2>Informatie over {{$info->name}}</h2>
				</div>
				<div class="profile-info-content">
					<div class="residence info-block"><strong>Leeftijd</strong><p ng-init="age = '{{$happyBirthDay}}'" ng-bind="age"></p></div>
					<div class="residence info-block"><strong>Woonplaats</strong><p ng-init="residence = '{{$info->residence}}'" ng-bind="residence"></p></div>
					<div class="school info-block"><strong>School</strong><p ng-init="school = '{{$info->school}}'" ng-bind="school"></p></div>
					<div class="work info-block"><strong>Werk</strong><p ng-init="work = '{{$info->work}}'" ng-bind="work"></p></div>
					<div class="bestfood info-block"><strong>Lievelings eten</strong><p ng-init="bestfood = '{{$info->bestfood}}'" ng-bind="bestfood"></p></div>
					<div class="hobbys info-block"><strong>Hobby's</strong><p ng-init="hobbys = '{{$info->hobbys}}'" ng-bind="hobbys"></p></div>
					<div class="email info-block"><strong>Email</strong><p ng-init="email = '{{$info->email}}'" ng-bind="email"></p></div>
					@if(Auth::user()->id == $profileId)
						<div id="edit-info" class="btn-primary btn edit-info" onclick="openInfoEdit()"><span class="btn-text">Bewerk informatie</span><img src="{{ asset('/img/icons/iconmonstr-pencil-10-icon-256.png') }}"></span></div>
					@endif;
				</div>
				<div class="btn-primary btn open-info info-btn" ng-if="infoOpen == false" ng-click="openAllInfo()"><span class="btn-text">Bekijk alle informatie</span><img src="{{ asset('/img/icons/iconmonstr-eye-5-icon-256.png') }}"></span></div>
				<div class="btn-primary btn close-info info-btn" ng-if="infoOpen == true" ng-click="closeAllInfo()"><span class="btn-text">Sluit alle informatie</span><img src="{{ asset('/img/icons/iconmonstr-x-mark-icon-256.png') }}"></span></div>
				
				<div class="profile-info-form">
					@if(Auth::user()->id == $profileId)
						<form id="profile-info-form" action="{{ route('editInfo', []) }}" method="post" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="profileId" value="{{ $profileId }}">								
								<div class="form-group">
									<label>Wat is je geboortedatum?:</label>
									<div class="row">
										<div class="col-xs-4">
										    <div id="select-month" class="input-group">
										    <span class="input-group-addon age" id="basic-addon1"><img src="{{ asset('/img/icons/info/iconmonstr-calendar-5-icon-256.png') }}"></span>
										      <div class="input-group-btn month">
										        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" name="month">
										        @foreach($months as $key => $month)
										        	@if($key + 1 == $birthMonth) {{$month}} @endif
										        @endforeach
										        @if($birthMonth == '') maand @endif
										        <span class="caret"></span>
										        </button>
										        <ul class="dropdown-menu" role="menu">
										        	@foreach($months as $key => $month)
										        		<li><a href="#" value="{{$key +1}}">{{$month}}</a></li>
										        	@endforeach
										        </ul>
										      </div><!-- /btn-group -->
										      <input class="hidden-month" type="hidden" name="month" value="{{$birthMonth}}">	
										    </div><!-- /input-group -->	
									    </div>

									    <div class="col-xs-4">
									    	<div id="select-day" class="input-group">
										      <input type="text" class="form-control day-input" maxlength="2" name="day" value ="{{$birthDay}}"placeholder="Dag">
										    </div><!-- /input-group -->	
									    </div>
									    <div class="col-xs-4">
									    	<div id="select-year" class="input-group">
										      <input type="text" class="form-control year-input" maxlength="4" name="year" value="{{$birthYear}}" placeholder="Jaar">
										    </div><!-- /input-group -->	
									    </div>
									</div>
								</div>

								<div id="residence-group" class="form-group">
									<label>Waar woon je?:</label>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1"><img src="{{ asset('/img/icons/info/iconmonstr-home-6-icon-256.png') }}"></span>
									    <input id="residence" class="form-control" type="text" aria-describedby="basic-addon1" class="residence" name="residence" value="{{$info->residence}}">
									</div>
								</div>	 
								<div id="work-group" class="form-group">
										<label>Wat voor werk doe je?:</label>
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon2"><img src="{{ asset('/img/icons/info/iconmonstr-briefcase-3-icon-256.png') }}"></span>
									    	<input id="work" class="form-control" type="text" class="work" name="work" value="{{$info->work}}">
										</div>
								</div>

								<div id="school-group" class="form-group">
										<label>Waar zit je op school?:</label>
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon3"><img src="{{ asset('/img/icons/info/iconmonstr-book-18-icon-256.png') }}"></span>
									    	<input id="school" class="form-control" type="text" class="school" name="school" value="{{$info->school}}">
										</div>
								</div>

								<div id="food-group" class="form-group">
										<label>Wat is je lievelingseten?:</label>
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon4"><img src="{{ asset('/img/icons/info/66466.png') }}"></span>
									    	<input id="bestfood" class="form-control" type="text" class="bestfood" name="bestfood" value="{{$info->bestfood}}">
										</div>
								</div>
								<div id="hobby-group" class="form-group">
										<label>Wat zijn jou hobby's?:</label>
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon5"><img src="{{ asset('/img/icons/info/iconmonstr-paintbrush-7-icon-256.png') }}"></span>
										    <input id="hobbys" class="form-control" type="text" class="hobbys" name="hobbys" value="{{$info->hobbys}}">
										</div>
								</div>

								<div id="email-group" class="form-group">
									<label>Email:</label>
									<div class="input-group">
										<span class="input-group-addon email" id="basic-addon6">@</span>
								    	<input id="email" class="form-control" type="text" class="email" name="email" value="{{$info->email}}">
									</div>
								</div>

								<div class="form-group">					
									<input id="submit-info"class="btn btn-primary submit-info" type="submit" value="Klaar">
								</div>
						</form>
					@endif
				</div>
				<div ng-if="loadingInfo == true">
					<div class="info-form-loading"><img src="{{ asset('/img/350.GIF') }}"></div>
				</div>
			</div> 
		</div>
	</div>
		
	
		<div class="row">
			<div class="post-heading">
				<h2>Berichten van {{$info->name}}</h2>
			</div>
			<div class="friends-overview-loading" ng-if="loading == true">
				<img src="{{ asset('/img/350.GIF') }}">
			</div>
			<div class="post-overview" ng-repeat='profilePost in profilePosts'>
				<div class="thumbnail">      
	                <div class="caption">
	                	<div class="post-user clearfix">
                    		<div>
                    			<a href="{{ url('profile/user')}}<% '/' + profileInfo.id %>">
	                    			<span class="post-friend-image" style="background : url({{ asset('/uploads/') }}/user_<%profileInfo.id%>/<%profileInfo.filename%>) top center; background-size:cover;"></span>
	                        	</a>
	                        	<div class="post-user-name"><a href="{{ url('profile/user')}}<% '/' + profileInfo.id %>"><strong><% profileInfo.name %></strong></a></div>
                        	</div>
	                    </div>
	                	<div class="post-desc">
	                    	<p><% profilePost.desc %></p>
	                    </div>
	                    <div ng-if="profilePost.filename.length != 0">
		                    <a href="#" data-featherlight="{{ asset('/uploads/') }}/user_<%profilePost.userId%>/<%profilePost.filename%>">
			                    <img ng-src="{{ asset('/uploads/') }}/user_<%profilePost.userId%>/<%profilePost.filename%>">
			                </a>
		                </div>
	                </div>
	            </div>
			</div>
		</div>
</div>
@endforeach

<div class="sidebar-right col-md-3">
	<div class="sidebar-photos">
	@foreach($profilePhotos as $key=>$profilePhoto)
		@if($key %2 === 0)
				<div class="row">
		@endif

			<div class="col-xs-6 photo">
				<div class="sidebar-image" style="background: url('{{ asset('/uploads/user_'.$profilePhoto->user_id.'/'.$profilePhoto->filename) }}') top center; background-size: cover;"></div>
			</div>

		@if($key %2 === 1)
			</div>
		@endif

	@endforeach
	</div>

	<div class="bs-photo" data-example-id="simple-nav-stacked">
		<ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
		  <li role="presentation" class="menu-item menu-item-photos clearfix ">
		  	<a href="{{route('profilePhotos', $profileId)}}">
		  		<span class="icon icon-photos" aria-hidden="true">
		  			<img src="http://localhost/laravel/public/img/icons/iconmonstr-picture-multi-icon-256.png">
		  		</span>
		  		<span class="menu-text">Alle foto's bekijken</span>
		  	</a>
		  </li>
		</ul>
	</div>
	
</div>

<!-- profileId to angular var -->
<script>
var profileId = {{$profileId}};
document.body.className = 'profile';
</script>
@endsection
