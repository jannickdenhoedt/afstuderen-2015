@extends('app')

@section('content')
<?php //action="{{ route('addPost', []) }}" ?>
		<div class="col-md-6" ng-app="socialPlatformApp" ng-cloak ng-controller="postsController">
			<tour step="publicMessage">	
				<div class="panel panel-default post-section">
					<div class="panel-heading"><h2>Openbaar bericht plaatsen</h2></div>
					<div class="panel-body">
						<div class="alert alert-warning" role="alert">
							<ul class="errors"></ul>
						</div>
						<div class="alert alert-succes" role="alert">
							<ul class="succes"></ul>
						</div>

						<form id="public-post" action="{{ route('addPost', []) }}" method="post" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div id="post-desc" class="form-group">				
								<textarea class="form-control post-message" name="descfield" placeholder="Vertel wat jij hebt gedaan"></textarea>
							</div>
								
							<div class="form-group">
								<div id="post-photo" class="fileUpload btn btn-primary">
								    <span><span class="btn-text">Foto bijvoegen</span><img src="{{ asset('/img/icons/iconmonstr-photo-camera-icon.png') }}"></span>
								    <input id="uploadBtn" type="file" class="upload" name="filefield">
								</div>	
								<span id="file-name"></span>
							</div>	

							<div class="post-loading"><img src="{{ asset('/img/350.GIF') }}"></div>		
								
					        
					        <div class="form-group">					
								<input id="submit-post" class="btn btn-success submit-post" type="submit" value="Bericht plaatsen"></input>
							</div>
					        
					    </form>
					</div>
				</div>
			</tour>

			<div class="col-md-12 post-overview-container">
				<div class="row">
				<div class="post-heading">
					<h2>Berichten van jouw vrienden</h2>
				</div>
				<div class="overview-loading" ng-if="loading == true">
					<img src="{{ asset('/img/350.GIF') }}">
				</div>
				<div class="post-overview" ng-repeat='post in posts'>
					<div class="thumbnail">      
	                    <div class="caption">
	                    	<div class="post-user clearfix" ng-repeat='user in users'>
	                    		<div ng-if="post.userId == user.id">
	                    			<a href="{{ url('profile/user')}}<% '/'+ user.id %>">
		                    			<span class="post-friend-image" style="background : url({{ asset('/uploads/') }}/user_<%user.id%>/<%user.filename%>) top center; background-size:cover;"></span>
		                        	</a>
		                        	<div class="post-user-name"><a href="{{ url('profile/user')}}<% '/' + user.id %>"><strong><% user.name %></strong></a></div>
	                        	</div>
	                    	</div>
	                    	<div class="post-date">
	                    			<p><% post.created_at %></p>
	                    		</div>
	                    	
	                    	<div class="post-desc">
	                        	<p><% post.desc %></p>
	                        </div>
	                        <div class="post-image" ng-if="post.filename.length != 0">
	                        	<a href="#" data-featherlight="{{ asset('/uploads/') }}/user_<%post.userId%>/<%post.filename%>">
	                        		<img ng-src="{{ asset('/uploads/') }}/user_<% post.userId+'/'+post.filename %>">
	                        	</a>
	                        </div>
	                    </div>
		            </div>
				</div>
				<div class="overview-loading" ng-if="lazyLoading == true">
					<img src="{{ asset('/img/350.GIF') }}">
				</div>
				<div ng-if="outOfPosts == true">
	                    <strong>Er zijn geen berichten meer</strong>
	            </div>

			</div>
			</div>
		</div>
		<!-- background color -->
		<script>
		document.body.className = 'home';
		</script>
@endsection