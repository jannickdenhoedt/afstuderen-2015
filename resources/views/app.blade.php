<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<title>Laravel</title>

	<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/featherlight.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/featherlight.gallery.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/bootstrap-tour-standalone.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/screen.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script src="{{ asset('/js/jquery-1.11.3.min.js') }}"></script>
	<script src="https://cdn.socket.io/socket.io-1.2.0.js"></script>
	
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"></a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar nav navbar-left">
					<li class="security">Wie kan mij zien? <input type="checkbox" name="security" checked></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				
				
					@if (Auth::guest())
					@else
						<li class="dropdown tours">
							<a href="#" class="dropdown-toggle clearfix" data-toggle="dropdown" role="button" aria-expanded="false"><span class="menu-text">Help</span><span class="icon"><img src="{{ asset('/img/icons/iconmonstr-help-2-icon-256.png') }}"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><label>Help me bij:</label></li>
								<li><a href="#" id="postTour">Bericht plaatsen</a></li>
								<li><a href="#" id="profileImageTour">Profiel foto bewerken</a></li>
								<li><a href="#" id="profileInfoTour">Profiel info bewerken</a></li>
								<li><a href="#" id="friendsTour">Vrienden bekijken</a></li>
								<li><a href="#" id="findFriendsTour">Vrienden vinden</a></li>
							</ul>
						</li>
						<li><a class="logout" href="{{ url('/auth/logout') }}">Uitloggen</a></li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			@if (!Auth::guest())
			<div class="bs-example col-md-3" data-example-id="simple-nav-stacked">
			    <ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
			      <li role="presentation" class="menu-item clearfix menu-item-overview {{Request::is('overzicht') ? 'active' : ''}}">
			      	<a href="{{ route('overview') }}">
			      		<span class="icon icon-overview" aria-hidden="true">
			      			<img src="{{ asset('/img/icons/iconmonstr-clipboard-6-icon-256.png') }}">
			      		</span>
			      		<span class="menu-text">Overzicht</span>
			      	</a>
			      </li>
			      <li role="presentation" class="menu-item clearfix menu-item-friends {{Request::is('vriendenboek') ? 'active' : ''}}">
			      	<a href="{{ route('friends') }}">
				      	<span class="icon icon-friends" aria-hidden="true">
				      		<img src="{{ asset('/img/icons/iconmonstr-user-14-icon-256.png') }}">
				      	</span> 
				      	<span class="menu-text">Vriendenboek</span>
			      	</a>
			      </li>
			      <li role="presentation" class="menu-item clearfix menu-item-newfriends {{Request::is('vind-vrienden') ? 'active' : ''}}">
				      <a href="{{ route('findFriends') }}">
					      <span class="icon icon-new-friends" aria-hidden="true">
					      	<img src="{{ asset('/img/icons/iconmonstr-user-11-icon-256.png') }}">
					      </span> 
					      <span class="menu-text">Vrienden maken</span>
				      </a>
			      </li>
			      <li role="presentation" class="menu-item clearfix menu-item-messages {{Request::is('gesprekken') ? 'active' : ''}}">
				      <a href="{{ route('chat') }}">
				      	<span class="icon icon-messages" aria-hidden="true">
				      		<img src="{{ asset('/img/icons/iconmonstr-speech-bubble-15-icon-256.png') }}">
				      	</span>
				      	<span class="menu-text">Gesprekken</span>
				      </a>
			      </li>
			      <li role="presentation" class="clearfix menu-profile menu-item {{Request::is('profile/user/'.Auth::user()->id) ? 'active' : ''}} " >
						<a href="{{ route('profile', Auth::user()->id) }}">
							<span class="menu-profile-image icon" style="background:url({{ asset('/uploads/user_').Auth::user()->id.'/'.Auth::user()->filename }}) top center; background-size:cover;"></span>
							<span class="menu-text">Mijn Profiel</span>
						</a>
			      </li>
			    </ul>
		  	</div>
		  		<!-- sent urls to tours -->
				<script>
					var baseUrl ='{{ url('/') }}';
					var userId = '{{ Auth::user()->id }}';
				</script>
		  	@endif
			
			
			@yield('content')
		</div>
	</div>

	<!-- Scripts -->
	<script src="http://malsup.github.com/jquery.form.js"></script>
	<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.12/angular.min.js"></script>
	<script src="{{ asset('/js/bootstrap-switch.min.js') }}"></script>
	<script src="{{ asset('/js/angular_app.js') }}"></script>
	<script src="{{ asset('/js/featherlight.js') }}"></script>
	<script src="{{ asset('/js/featherlight.gallery.js') }}"></script>
	<script src="{{ asset('/js/bootstrap-tour-standalone.js') }}"></script>
	<script src="{{ asset('/js/config.js') }}"></script>

	<script type="text/javascript">
	$.fn.bootstrapSwitch.defaults.onText = 'Iedereen';
	$.fn.bootstrapSwitch.defaults.offText = 'Vrienden';
	$("[name='security']").bootstrapSwitch();
	</script>
		
</body>
</html>
