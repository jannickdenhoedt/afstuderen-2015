<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

//overview page
Route::get('overzicht', array('as'=>'overview', 'uses'=>'HomeController@index'));
 
 //public post routes
Route::get('getAllPosts', ['as' => 'getAllPosts', 'uses' => 'OverviewPostController@index']);
Route::post('addPost/add',['as' => 'addPost', 'uses' => 'OverviewPostController@add']);
Route::get('lazyLoadPosts/{id}', array('as'=>'lazyLoad', 'uses'=>'OverviewPostController@lazyLoad'));

//friends page
Route::get('vriendenboek', array('as'=>'friends', 'uses'=>'FriendsController@index'));
Route::get('getFriends', array('as'=>'getFriends', 'uses'=>'FriendsController@getFriends'));

//new friends page
Route::get('vind-vrienden', array('as'=>'findFriends', 'uses'=>'FindFriendsController@index'));
Route::get('findFriends', array('as'=>'getNewFriends', 'uses'=>'FindFriendsController@getNewFriends'));
Route::post('sentInvitation', array('as'=>'sentInvitation', 'uses'=>'FindFriendsController@sentInvitation'));
Route::get('getInvitedFriends', array('as'=>'getInvitedFriends', 'uses'=>'FindFriendsController@getInvitedFriends'));
Route::post('acceptInvitation', array('as'=>'acceptInvitation', 'uses'=>'FindFriendsController@acceptInvitation'));

//profile page
Route::get('profile/user/{id}', array('as'=>'profile', 'uses'=>'ProfileController@index'));
Route::get('getProfile/{id}', array('as'=>'getProfile', 'uses'=>'ProfileController@getProfile'));
Route::post('profile/addImage', array('as'=>'addProfileImage', 'uses'=>'ProfileController@addProfileImage'));
Route::get('profile/profileImage', array('as'=>'getProfileImage', 'uses'=>'ProfileController@getProfileImage'));
Route::post('profile/editInfo', array('as'=>'editInfo', 'uses'=>'ProfileController@editInfo'));
	//profile page photos
	Route::get('profile/user/{id}/fotos', array('as'=>'profilePhotos', 'uses'=>'ProfileController@photos'));
	Route::get('profile/user/{id}/getPhotos', array('as'=>'getProfilePhotos', 'uses'=>'ProfileController@getPhotos'));

//chat page
Route::get('gesprekken', array('as'=>'chat', 'uses'=>'ChatController@index'));
Route::get('getChat/{id}', array('as'=>'getChat', 'uses'=>'ChatController@getChat'));
Route::get('getMostRecentChat', array('as'=>'getMostRecentChat', 'uses'=>'ChatController@getMostRecentChat'));
Route::post('addMessage', array('as'=>'addMessage', 'uses'=>'ChatController@postMessages'));
Route::get('getCurrentProfile', array('as'=>'getCurrentProfile', 'uses'=>'ChatController@getCurrentProfile'));

//authentication
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::post('register', array('as'=>'register', 'uses'=>'AuthController@register'));
