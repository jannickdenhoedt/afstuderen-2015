<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Request;
use App\Messages;
use App\User;


class ChatController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	//create chat home view
	public function index()
	{
		$currentUserId = \Auth::user()->id;

		return view('chat', array('profileId' => $currentUserId));
	}

	//get current profile for chat
	public function getCurrentProfile(){
		$currentUserId = \Auth::user()->id;
		$currentProfile = User::where('id', '=', $currentUserId)->get();

		return $currentProfile;
	}

	//get chat when clicked 
	public function getChat($id){
		$currentUserId = \Auth::user()->id;
		$profileId = $id;


		//get all chatmessages for current chat
		$chatMessages = Messages::where(function ($query) use ($currentUserId) {
		    $query->where('senderId', '=', $currentUserId)
		          ->orWhere('receiverId', '=', $currentUserId);
		})->where(function ($query) use ($profileId) {
		    $query->where('senderId', '=', $profileId)
		          ->orWhere('receiverId', '=', $profileId);
		})->get();

		$receiverUser = User::where('id', '=', $profileId)->get();
		
		return ['messages' => $chatMessages, 'receiverUser' => $receiverUser];
	}

	//get most recent chat when load the chat page
	public function getMostRecentChat(){
		$currentUserId = \Auth::user()->id;
		$profileId;

		//get first message from db
		$recentMessage = Messages::where(function ($query) use ($currentUserId) {
		    $query->where('senderId', '=', $currentUserId)
		          ->orWhere('receiverId', '=', $currentUserId);
		})->orderBy('created_at', 'desc')->first();


		if($recentMessage != null){
			if($recentMessage->senderId == $currentUserId){
			$profileId = $recentMessage->receiverId;
			}else{
				$profileId = $recentMessage->senderId;
			}
		}else{
			return 'empty';
		}
		
		// get chat messages for most recent chat
		$chatMessages = Messages::where(function ($query) use ($currentUserId) {
	    $query->where('senderId', '=', $currentUserId)
	          ->orWhere('receiverId', '=', $currentUserId);
		})->where(function ($query) use ($profileId) {
		    $query->where('senderId', '=', $profileId)
		          ->orWhere('receiverId', '=', $profileId);
		})->get();

		$receiverUser = User::where('id', '=', $profileId)->get();

		return ['messages' => $chatMessages, 'receiverUser' => $receiverUser];
	}

	//post message in db
	public function postMessages(){

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$message = $request->message;
		$currentUserId = $request->senderId;
		$receiverId = $request->receiverId;

		$messages = new Messages();

		$messages->message = $message;
		$messages->senderId = $currentUserId;
		$messages->receiverId = $receiverId;

		$messages->save();

		return 'succes';
	}

}
