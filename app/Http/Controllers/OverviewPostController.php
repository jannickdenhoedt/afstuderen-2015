<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\UserPost;
use App\User;
use App\Photos;
use Request;
use Image;
 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
 
class OverviewPostController extends Controller {
 
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	//create overview homepage view
	public function index() {
 
		$currentUserId = \Auth::user()->id;

		$userFriendsArray = array($currentUserId);

		//get all usersfriends
		$userFriendsQuery = User::where('id', '=', $currentUserId)->select('friends')->get();
		$userFriendsDecoded = json_decode($userFriendsQuery);
		$userFriends = unserialize($userFriendsDecoded[0]->friends);

		for ($i=0; $i < count($userFriends); $i++) { 
			$userFriendsArray[] = $userFriends[$i];
		}
		
		//get first 10 posts for overview	
		$userFriendsPosts = UserPost::whereIn('userId', $userFriendsArray)->orderBy('created_at', 'desc')->take(10)->get();
		$userFriends = User::whereIn('id', $userFriendsArray)->get();

		
		return array('posts' => $userFriendsPosts, 'users' => $userFriends);
	}



	//lazyload extra posts
	public function LazyLoad($id){
		$currentUserId = \Auth::user()->id;
		$skipPosts = $id * 10;
		$userFriendsArray = array($currentUserId);

		$userFriendsQuery = User::where('id', '=', $currentUserId)->select('friends')->get();
		$userFriendsDecoded = json_decode($userFriendsQuery);
		$userFriends = unserialize($userFriendsDecoded[0]->friends);

		for ($i=0; $i < count($userFriends); $i++) { 
			$userFriendsArray[] = $userFriends[$i];
		}
		
		$userFriendsPosts = UserPost::whereIn('userId', $userFriendsArray)->orderBy('created_at', 'desc')->skip($skipPosts)->take(10)->get();
		$userFriends = User::whereIn('id', $userFriendsArray)->get();

		return array('posts' => $userFriendsPosts, 'users' => $userFriends);
	}

	//add public post
	public function add() {

		$currentUserId = \Auth::user()->id;
		$file = Input::file('filefield');
		$desc = Request::input('descfield');

		$rules = array(
			'descfield' => 'required',
			'filefield' => '|image|mimes:jpeg,png'
			);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
		    return ['success' => false, 'message' => $validator->errors()->all()];
		}
		else
		{
			$userPost = new UserPost();
			$photos = new Photos();

			//get image, compress image, save image
			if($file != null){
				$destinationPath = 'uploads/user_'.$currentUserId; // upload path
				$extension = $file->getClientOriginalExtension(); // getting image extension
				$fileName = rand(11111,99999).'.'.$extension;
				$file->move($destinationPath, $fileName); // uploading file to given path

				//resize the image
				$img = Image::make($destinationPath.'/'.$fileName);
				$img->resize(700, null, function ($constraint) {
				    $constraint->aspectRatio();
				    $constraint->upsize();
				});
				$img->save();

				$userPost->mime = $file->getClientMimeType();
				$userPost->original_filename = $file->getClientOriginalName();
				$userPost->filename = $fileName;

				$photos->mime = $file->getClientMimeType();
				$photos->original_filename = $file->getClientOriginalName();
				$photos->filename = $fileName;
				$photos->user_id = $currentUserId;
				$photos->save();
			}

			//save post desc to db
			$userPost->desc = $desc;
			$userPost->userId = $currentUserId;

			$userPost->save();


			return ['success' => true, 'message' => "Het bericht is succesvol geplaatst"];
		}	
	}


}