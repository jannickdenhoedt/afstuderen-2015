<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Request;
use App\User;


class FriendsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	//index friends
	public function index()
	{		
		return view('friends');
	}

	//get all friends
	public function getFriends(){
		$currentUserId = \Auth::user()->id;
		$userFriendsArray = array();

		$userFriendsQuery = User::where('id', '=', $currentUserId)->select('friends')->get();
		$userFriendsDecoded = json_decode($userFriendsQuery);
		$userFriends = unserialize($userFriendsDecoded[0]->friends);

		for ($i=0; $i < count($userFriends); $i++) { 
			$userFriendsArray[] = $userFriends[$i];
		}

		$userFriends = User::whereIn('id', $userFriendsArray)->orderBy('name', 'asc')->get();

		return $userFriends;
	}

}
