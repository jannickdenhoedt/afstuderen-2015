<?php namespace App\Http\Controllers;
//require 'vendor/autoload.php';
use Illuminate\Support\Facades\Input;
use Request;
use App\User;
use App\Photos;
use App\UserPost;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;



class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	//index homepage
	public function index($id)
	{		
		$profileId = $id;
		$profileInfo = User::where('id', '=', $profileId)->get();
		$profilePhotos = Photos::where('user_id', '=', $profileId)->orderBy('created_at', 'desc')->take(4)->get();
		$monthsArray = array('januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december');
		//parse birth date
		foreach ($profileInfo as $key => $info){
			if($info->date_birth != '0000-00-00'){
				$dt = Carbon::parse($info->date_birth);
				$year = $dt->year;
				$month = $dt->month;
				$day = $dt->day;
				$birthDay = Carbon::createFromDate($year, $month, $day)->age;
			}
			else{
				$year = '';
				$month = '';
				$day = '';
				$birthDay = '';
			}
			
		}

		return view('profile', array('profileId'=>$profileId, 'profileInfo' => $profileInfo, 'profilePhotos' => $profilePhotos, 'months' => $monthsArray, 'birthYear' => $year, 'birthMonth' => $month, 'birthDay' => $day, 'happyBirthDay' => $birthDay));
	}

	//get all profile info
	public function getProfile($profileId){

		$profileInfo = User::where('id', '=', $profileId)->get();
		$profilePosts = UserPost::where('userId', '=', $profileId)->orderBy('created_at', 'desc')->get();
		$profileData = $profilePosts;

		return ['profileInfo' => $profileInfo, 'profilePosts' => $profilePosts];
	}

	//add profile image
	public function addProfileImage(){

		$currentUserId = \Auth::user()->id;
		$file = Input::file('filefield');

		$rules = array('filefield' => 'required|image|mimes:jpeg,png');
		$validator = Validator::make(Input::all(), $rules);

		

		//get image, compress image and save image to db.
		if ($validator->fails())
		{
		    return ['success' => false, 'message' => $validator->errors()->all()];
		}
		else
		{
			$photos = new Photos();

			if($file != null){
				$destinationPath = 'uploads/user_'.$currentUserId; // upload path
				$extension = $file->getClientOriginalExtension(); // getting image extension
				$fileName = 'profile_'.rand(11111,99999).'.'.$extension; // renameing image
				$file->move($destinationPath, $fileName); // uploading file to given path

				DB::table('users')
		            ->where('id', $currentUserId)
		            ->update([
		            	'filename' => $fileName,
		            	'mime' => $file->getClientMimeType(),
		            	'original_filename' => $file->getClientOriginalName()
		            	]);

		        $photos->mime = $file->getClientMimeType();
				$photos->original_filename = $file->getClientOriginalName();
				$photos->filename = $fileName;
				$photos->user_id = $currentUserId;
				$photos->save();
			}

			//return succes if image uploaded
			return ['success' => true, 'message' => "Het bericht is succesvol geplaatst"];
		}	
	}

	//get profileimage
	public function getProfileImage(){

		$currentUserId = \Auth::user()->id;
		$profileInfo = User::where('id', '=', $currentUserId)->select('filename')->get();


		return  $profileInfo[0]['filename'];
	}

	///edit info
	public function editInfo(){

		$currentUserId = \Auth::user()->id;

		//get all input
		$email = Request::input('email');
		$residence = Request::input('residence');
		$work = Request::input('work');
		$hobbys = Request::input('hobbys');
		$school = Request::input('school');
		$bestfood = Request::input('bestfood');

		$month = Request::input('month');
		$day = Request::input('day');
		$year = Request::input('year');
		$dateBirthStr = $year.'-'.$month.'-'.$day;

		if(!empty($month) && !empty($day) && !empty($year)){
			$dateBirthStmp = \Carbon\Carbon::createFromFormat('Y-m-d', $dateBirthStr);
			$birthDay = Carbon::createFromDate($year, $month, $day)->age;

		}else{
			$dateBirthStmp = '00-00-0000';
			$birthDay = '';
		}

		//put data in array
		$infoArray = array('email' => $email, 'residence' => $residence,'work' => $work, 'hobbys' => $hobbys, 'school' => $school, 'bestfood' => $bestfood, 'age' => $birthDay);

		$rules = array('email' => 'required|email');
		$validator = Validator::make(Input::all(), $rules);

		//validate data
		if ($validator->fails())
		{
		    return ['success' => false, 'message' => $validator->errors()->all()];
		}
		else
		{
			//put data in db
			DB::table('users')
	            ->where('id', $currentUserId)
	            ->update([
	            	'email' => $email,
	            	'residence' => $residence,
	            	'work' => $work,
	            	'hobbys' => $hobbys,
	            	'school' => $school,
	            	'bestfood' => $bestfood,
	            	'date_birth' => $dateBirthStmp
	            	]);

	        return ['success' => true, 'message' => "Info is succesvol aangepast", 'infoArray' => $infoArray];
    	}
	}

	//get all profile images
	public function photos($id){

		$profileId = $id;
		$profileInfo = User::where('id', '=', $profileId)->get();
		
		return view('profile-photos', array('profileId'=>$profileId, 'profileInfo' => $profileInfo));
	
	}

	//get all profile images of current profile
	public function getPhotos($id){

		$profileId = $id;
		$profilePhotos = Photos::where('user_id', '=', $profileId)->orderBy('created_at', 'desc')->get();
		
		return $profilePhotos;
	}

}
