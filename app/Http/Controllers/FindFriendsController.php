<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Request;
use App\User;
use App\UserPost;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class FindFriendsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	//index homepage
	public function index()
	{		
		return view('find-friends');
	}

	//get all potential friends
	public function getNewFriends(){
		$currentUserId = \Auth::user()->id;

		$userNewFriendsArray = array($currentUserId);
		$userInvitedArray = array();

		//get sentinvititaions
		$usersInvitedQuery = User::where('id', '=', $currentUserId)->select('sentinvitations')->get();
		$usersInvitedDecoded = json_decode($usersInvitedQuery);
		$usersInvited = unserialize($usersInvitedDecoded[0]->sentinvitations);

		//get invitations
		$invitedQuery = User::where('id', '=', $currentUserId)->select('invitations')->get();
		$invitedDecoded = json_decode($invitedQuery);
		$invited = unserialize($invitedDecoded[0]->invitations);

		//get all new friends
		$userFriendsQuery = User::where('id', '=', $currentUserId)->select('friends')->get();
		$userFriendsDecoded = json_decode($userFriendsQuery);
		$userFriends = unserialize($userFriendsDecoded[0]->friends);

		//put all data in arrays
		if($userFriends != false){
			for ($i=0; $i < count($userFriends); $i++) { 
				$userNewFriendsArray[] = $userFriends[$i];
			}
		}

		if($invited != false){
			for ($i=0; $i < count($invited) ; $i++) { 
				$userInvitedArray[] = $invited[$i];
				$userNewFriendsArray[] = $invited[$i];

			}
		}

		if($usersInvited != false){
			for ($i=0; $i < count($usersInvited) ; $i++) { 
				$userNewFriendsArray[] = $usersInvited[$i];

			}
		}

		//get all data from database
		$userNewFriends = User::whereNotIn('id', $userNewFriendsArray)->get();
		$userInvitations = User::whereIn('id', $userInvitedArray)->get();
		

		return ['newFriends' => json_decode($userNewFriends), 'invited' => $usersInvited, 'invitations' => json_decode($userInvitations)];
	}

	public function sentInvitation(){

		$currentUserId = \Auth::user()->id;

		///-----------------------------------///
		//---set invitation to current user---///
		///-----------------------------------///
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$id = $request->id;

		$sentInvitations = User::where('id', '=', $currentUserId)->select('sentinvitations')->get();
		$sentInvitationsArr = json_decode($sentInvitations);
		$sentInvitationsArrUnserialised = unserialize($sentInvitationsArr[0]->sentinvitations);
		
		$sentInvitationsArrUnserialised[] = $id;

		DB::table('users')
            ->where('id', $currentUserId)
            ->update([
            	'sentinvitations' => serialize($sentInvitationsArrUnserialised)
            	]);


        
        ///-----------------------------------///
		//---set invitation to invited user---///
		///-----------------------------------///

        $Invitations = User::where('id', '=', $id)->select('invitations')->get();
		$InvitationsArr = json_decode($Invitations);
		$InvitationsArrUnserialised = unserialize($InvitationsArr[0]->invitations);
		
		$InvitationsArrUnserialised[] = $currentUserId;

		DB::table('users')
            ->where('id', $id)
            ->update([
            	'invitations' => serialize($InvitationsArrUnserialised)
            	]);

		return 'success';
	}



	//get all invited friends
	public function getInvitedFriends(){
		$currentUserId = \Auth::user()->id;

		$usersInvitedQuery = User::where('id', '=', $currentUserId)->select('sentinvitations')->get();
		$usersInvitedDecoded = json_decode($usersInvitedQuery);
		$usersInvited = unserialize($usersInvitedDecoded[0]->sentinvitations);

		return $usersInvited;
	}



	//get all invitations 
	public function acceptInvitation(){
		//current user id
		$currentUserId = \Auth::user()->id;
		
		//accept user id
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$id = $request->id;

		
		///-----------------------------------///
		//---update currentuser friends-------///
		///-----------------------------------///
		$userFriendsQuery = User::where('id', '=', $currentUserId)->select('friends')->get();
		$userFriendsDecoded = json_decode($userFriendsQuery);
		$userFriendsSerial = unserialize($userFriendsDecoded[0]->friends);

		if(!empty($userFriendsSerial)){
			$userFriends = array_values($userFriendsSerial);
		}else{
			$userFriends = $userFriendsSerial;
		}

		$userFriends[] = $id;

        ///-----------------------------------///
		//---update acceptuser friends--------///
		///-----------------------------------///
		$AcceptUserFriendsQuery = User::where('id', '=', $id)->select('friends')->get();
		$AcceptUserFriendsDecoded = json_decode($AcceptUserFriendsQuery);
		$AcceptUserFriendsSerial = unserialize($AcceptUserFriendsDecoded[0]->friends);

		if(!empty($AcceptUserFriendsSerial)){
			$AcceptUserFriends = array_values($AcceptUserFriendsSerial);
		}else{
			$AcceptUserFriends = $AcceptUserFriendsSerial;
		}
				
		$AcceptUserFriends[] = $currentUserId;

		///-----------------------------------///
		//--------update sentinvitations------///
		///-----------------------------------///
		$userSentInvitationQuery = User::where('id', '=', $id)->select('sentinvitations')->get();
		$userSentInvitationDecoded = json_decode($userSentInvitationQuery);
		$userSentInvitationSerial = unserialize($userSentInvitationDecoded[0]->sentinvitations);

		if(!empty($userSentInvitationSerial)){
			$userSentInvitation = array_values($userSentInvitationSerial);
		}else{
			$userSentInvitation = $userSentInvitationSerial;
		}

		for ($i=0; $i < count($userSentInvitation); $i++) { 
			if($currentUserId == $userSentInvitation[$i]){
				unset($userSentInvitation[$i]);
			}
		}

		$userSentInvitation = array_values($userSentInvitation);

        ///-----------------------------------///
		//----------update invitations -------///
		///-----------------------------------///
		$usersInvitedQuery = User::where('id', '=', $currentUserId)->select('invitations')->get();
		$usersInvitedDecoded = json_decode($usersInvitedQuery);
		$usersInvitedSerial = unserialize($usersInvitedDecoded[0]->invitations);
		if(!empty($usersInvitedSerial)){
			$usersInvited = array_values($usersInvitedSerial);
		}else{
			$usersInvited = $usersInvitedSerial;
		}

		for ($i=0; $i < count($usersInvited); $i++) { 

			if($id == $usersInvited[$i]){
				unset($usersInvited[$i]);
			}
		}

		//put all data in db
		$usersInvited = array_values($usersInvited);

		DB::table('users')
            ->where('id', $currentUserId)
            ->update([
            	'friends' => serialize($userFriends)
            	]);

        DB::table('users')
	        ->where('id', $id)
	        ->update([
	        	'friends' => serialize($AcceptUserFriends)
	        	]);

	    DB::table('users')
            ->where('id', $id)
            ->update([
            	'sentinvitations' => serialize($userSentInvitation)
            	]);

		DB::table('users')
            ->where('id', $currentUserId)
            ->update([
            	'invitations' => serialize($usersInvited)
            	]);

		return ['succes' => true];
		
	}

}
