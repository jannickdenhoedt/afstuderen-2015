if(document.getElementById('uploadBtn')){
	document.getElementById("uploadBtn").onchange = function () {
    	$('#file-name').html(this.value.replace("C:\\fakepath\\", ""));
	};
}


$(document).ready(function(){

	//basic dom manipulation
	$('.sidebar-photos .photo').each(function(){
		$(this).css({'height': $(this).width()});
	})

	//select date of birth
	$('.profile-info-form .input-group-btn .dropdown-menu li a').click(function(event){
		event.preventDefault();
		$('.profile-info-form .input-group-btn button').html($(this).html() + '<span class="caret"></span>');
		$('.profile-info-form .hidden-month').val($(this).attr('value'));
	})

	//birth only accept integers
    $(".year-input, .day-input").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

	//resize function
	$(window).resize(function(){
		$('.sidebar-photos .photo').each(function(){
			$(this).css({'height': $(this).width()});
		})
	})

	/////////////////////////
	/////welcome tour////////
	window.startingTour = new Tour({
		name: 'startingTour',
		template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button></div> </div>',
		onShown: function (tour) { 
			$('.tour-startingTour-element input').select();
			$('.popover-content').hide();

		    $(".tour-startingTour-element").on("input", function() {
		       $('.popover-content').show();
		    });

		    //disable enter
		    $(".tour-startingTour-element input").keydown(function (e) {
		        if ((e.keyCode == 13))  {return false;}
		    });

		    $('#edit-info').click(function(){
				window.startingTour.next();
			})

		    $('#file-name').bind("DOMSubtreeModified",function(){
			  window.startingTour.next();
			});


			if(window.startingTour.getCurrentStep() == 2){
				$('#select-month .month').addClass('open');
				$('#select-month .dropdown-toggle').attr('aria-expanded', 'true');
			}	
		}
	});


	//adding steps
	addEditProfileImageSteps(window.startingTour);
	addEditProfileInfoSteps(window.startingTour);
	addOverviewPostSteps(window.startingTour);
	addFindFriendsSteps(window.startingTour);
	addFriendsSteps(window.startingTour);

	window.startingTour.init();
	window.startingTour.start();	
	
	///////////////////////////////
	/////post overview tour////////
	window.overviewPostTour = new Tour({
		name: 'overviewPostTour',
		template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-primary btn-prev" data-role="prev">Vorige</button> <button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> <button class="btn btn-sm btn-danger btn-done" data-role="end">Klaar</button> </div> </div>',
		onShown: function (tour) { 
			
		}
	});
	addOverviewPostSteps(window.overviewPostTour);

	window.overviewPostTour.init();
	$('#postTour').click(function(event){
		window.overviewPostTour.restart();
	});

	///////////////////////////////
	//////////profile image tour////////
	profileImageTour = new Tour({
		name: 'profileImageTour',
		template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-primary btn-prev" data-role="prev">Vorige</button> <button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> <button class="btn btn-sm btn-danger btn-done" data-role="end">Klaar</button> </div> </div>',
		onShown: function (tour) { 
			$('#file-name').bind("DOMSubtreeModified",function(){
			  window.profileImageTour.end();
			});
		}
	});
	addEditProfileImageSteps(profileImageTour);

	profileImageTour.init();
	$('#profileImageTour').click(function(event){
		profileImageTour.restart();
	});

	///////////////////////////////
	//////////profile info tour////////
	profileInfoTour = new Tour({
		name: 'profileInfoTour',
		template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-primary btn-prev" data-role="prev">Vorige</button> <button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> <button class="btn btn-sm btn-danger btn-done" data-role="end">Klaar</button> </div> </div>',
		onShown: function (tour) { 
			$('#edit-info').click(function(){
				profileInfoTour.next();
			})

			$('#submit-info').click(function(){
				profileInfoTour.end();
			})
		}
	});
	addEditProfileInfoSmallSteps(profileInfoTour);

	profileInfoTour.init();
	$('#profileInfoTour').click(function(event){
		profileInfoTour.restart();
	});
	$('#submit-info').click(function(event){
		profileInfoTour.end();
		
	});
	

	///////////////////////////////
	//////////friends tour////////

	window.friendsTour = new Tour({
		name: 'friendsTour',
		template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-primary btn-prev" data-role="prev">Vorige</button> <button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> <button class="btn btn-sm btn-danger btn-done" data-role="end">Klaar</button> </div> </div>'
	});
	addFriendsSteps(window.friendsTour);

	window.friendsTour.init();

	$('#friendsTour').click(function(event){
		window.friendsTour.restart();
	});

	///////////////////////////////
	//////////find friends tour////////

	findFriendsTour = new Tour({
		name: 'findFriendsTour',
		template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-primary btn-prev" data-role="prev">Vorige</button> <button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> <button class="btn btn-sm btn-succes btn-done" data-role="end">Klaar</button> </div> </div>'
	});
	addFindFriendsSmallSteps(findFriendsTour);

	findFriendsTour.init();
	$('#findFriendsTour').click(function(event){
		findFriendsTour.restart();
	});

})

function addEditProfileImageSteps(tour){
	tour.addSteps([
		{
			element: "#file-upload-btn",
			title: "Klik op de blauwe knop en kies een foto van jezelf",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId,
		}
	])

}

function addEditProfileInfoSteps(tour){
	tour.addSteps([
		{
			element: "#edit-info",
			title: "Klik op de blauwe knop om informatie te bewerken",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId,
			onNext: function(){
				openInfoEdit();
			}
		},
		{
			element: "#select-month",
			title: "Kies hier je geboorte maand",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#select-day",
			title: "Vul hier de dag in dat je geboren bent",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#select-year",
			title: "Vul hier het jaar in dat je geboren bent",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#residence-group",
			title: "Vul hier je woonplaats in",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#work-group",
			title: "Vul hier in wat voor werk je doet",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#school-group",
			title: "Vul hier in op welke school je zit",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#food-group",
			title: "Vul je lievelingseten in",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#hobby-group",
			title: "Vul je hobby's in",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#email-group",
			title: "Vul je email adres in",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId
		},
		{
			element: "#submit-info",
			title: "Klik op de blauwe knop om de informatie te bewaren",
			content: "Klik op klaar",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId,
			template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> </div> </div>'
	
		}
	])
}

function addEditProfileInfoSmallSteps(tour){
	tour.addSteps([
		{
			element: "#edit-info",
			title: "Klik op de blauwe knop om informatie te bewerken",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId,
			onNext: function(){
				openInfoEdit();
			}
		},
		{
			element: "#info-body",
			title: "Bewerk hier jouw informatie",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId,
			placement: 'top'
		},
		{
			element: "#submit-info",
			title: "Klik op de blauwe knop om de informatie te bewaren",
			content: "Klik op klaar",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/profile/user/'+userId,
		}
	])
}

function addOverviewPostSteps(tour){
	tour.addSteps([
		{
			element: "#post-desc",
			title: "Schrijf een bericht aan al je vrienden",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/overzicht'
		},
		{
			element: "#post-photo",
			title: "Klik op de blauwe knop om foto te kiezen",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/overzicht'
		},
		{
			element: "#submit-post",
			title: "Klik op de groene knop om het bericht te plaatsen",
			content: "Klik op klaar als je het bericht hebt geplaatst",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/overzicht',
			template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> </div> </div>'
	
		}
	])
}

function addFindFriendsSteps(tour){
	tour.addSteps([
		{
			element: "#friendInvites",
			title: "Hier staan uitnodigingen van vrienden",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/vind-vrienden',
			template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content friends-content"></div> <div class="popover-navigation"> <div class="btn-group"><button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> </div> </div>'
	
		},
		{
			element: "#findFriends",
			title: "Hier vind je mensen om vrienden mee te worden",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/vind-vrienden',
			placement:'top',
			name: "find-friends",
			template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content friends-content"></div> <div class="popover-navigation"> <div class="btn-group"><button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> </div> </div>'
	
		}
	])
}

function addFindFriendsSmallSteps(tour){
	tour.addSteps([
		{
			element: "#friendInvites",
			title: "Hier staan uitnodigingen van vrienden",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/vind-vrienden',
			template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content friends-content"></div> <div class="popover-navigation"> <div class="btn-group"><button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> </div> </div>'
	
		},
		{
			element: "#findFriends",
			title: "Hier vind je mensen om vrienden mee te worden",
			content: "Klik op volgende om verder te gaan",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/vind-vrienden',
			placement:'top',
			name: "find-friends",
		}
	])
}

function addFriendsSteps(tour){
	tour.addSteps([
		{
			element: "#friends",
			title: "Hier vind je al je vrienden",
			content: "Als je nog geen vriend hebt moet je eerst een vriend maken",
			backdrop: true,
			backdropContainer: 'body',
			backdropPadding: 10,
			path: '/laravel/public/vriendenboek',
			template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-primary btn-prev" data-role="prev">Vorige</button> <button class="btn btn-sm btn-success btn-next" data-role="next">Volgende</button> <button class="btn btn-sm btn-success btn-done" data-role="end">Klaar</button> </div> </div>'
	
		}
	])
}

function reInitTour(tour){
	tour.init();
}
