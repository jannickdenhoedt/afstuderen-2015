var app = angular.module('socialPlatformApp', []);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
  });
 
app.controller('postsController', function($scope, $http) {
 
	$scope.posts = [];
	$scope.loading = false;
	var startLazyLoad = false;
	var skipPostId = 1;
 
	$scope.init = function() {
		$scope.loading = true;
		$http.get('/laravel/public/getAllPosts').
		success(function(data, status, headers, config) {
			$scope.posts = data.posts;
			$scope.users = data.users;
			$scope.loading = false; 
			$scope.lazyLoading = false;
			$scope.outOfPosts = false;
			startLazyLoad = true;
		});
	}
	$scope.init();

	$scope.lazyLoad = function(){
		if(startLazyLoad == true){
			startLazyLoad = false;
			$scope.lazyLoading = true;

			$http.get('/laravel/public/lazyLoadPosts/'+skipPostId).
			success(function(data, status, headers, config) {
				console.log(skipPostId);
				for (var i = 0; i < data.posts.length; i++) {
					$scope.posts.push(data.posts[i]);
				};

				if(data.posts.length != 0){
					skipPostId ++;	
					startLazyLoad = true;
				}else{
					$scope.outOfPosts = true;
				}
				
				$scope.lazyLoading = false;
			});
		}
	}

	//----------------------------
	//Public post upload ajaxform
	//----------------------------

	$('#public-post').ajaxForm({ 
        dataType:  'json', 
        beforeSubmit:  startLoader,
        success:   processJson 
    });

    //----------------------------
	//Public post upload callbacks
	//----------------------------

	function startLoader(){
	    $('.post-loading').show();
	}

	function processJson(data){
	    $('.post-loading').hide();
	    if(data.success == false){
	        $('.alert-warning').show();
	        $('.alert-succes').hide();
	        $('.errors').html('');
	        for (var i = data.message.length - 1; i >= 0; i--) {
	            $('.errors').append('<li>'+data.message[i]+'</li>');

	            if($('.tour-startingTour').length > 0){
	        		window.startingTour.goTo(12);
	        	}
	        	if($('.tour-overviewPostTour').length > 0){
	        		window.overviewPostTour.goTo(0);
	        	}
	        };  
	        reInitTour(window.overviewPostTour);
	    }

	    if(data.success == true){
	        $('.alert-warning').hide();
	        $('.alert-succes').show();
	        $('succes').html('');
	        $('.succes').append('<li>'+data.message+'</li>');

	        if($('.tour-startingTour').length > 0){
	        	window.startingTour.next();
	        }

	        if($('.tour-overviewPostTour').length > 0){
	        	window.overviewPostTour.end();
	        }

	        $scope.init();
	    }
	} 

	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
			$scope.lazyLoad();
		}
	});
 
});

app.controller('friendsController', function($scope, $http) {
 
	$scope.posts = [];
	$scope.loading = false;
 
	$scope.init = function() {
		$scope.loading = true;
		$http.get('/laravel/public/getFriends').
		success(function(data, status, headers, config) {
			$scope.friends = data;
				$scope.loading = false; 
				reInitTour(window.friendsTour);
				reInitTour(window.startingTour);
		});
	}
	$scope.init();

	$scope.stopFriendsTour = function(){
		window.friendsTour.end();
	}
 
});

app.controller('findFriendsController', function($scope, $http) {
 
	$scope.posts = [];
	$scope.loading = false;
	$scope.invited;
	$scope.invitedId;
 
	$scope.init = function() {
		$scope.loading = true;
		$http.get('/laravel/public/findFriends').
		success(function(data, status, headers, config) {
			$scope.friends = data.newFriends;
			$scope.invited = data.invited;
			console.log(data.invitations);
			$scope.invitations = data.invitations;
			$scope.loading = false; 
			reInitTour(window.startingTour);
		});
	}
	$scope.init();

	$scope.inviteFriend = function(id){
		$http.post('/laravel/public/sentInvitation', { 'id' : id }).
		success(function(data, status, headers, config) {
			if(data == 'success'){
				$scope.init();
				$scope.getInvited();
				$('.friend-invite-succes').show();
				reInitTour(window.startingTour);
			}
		});
	}

	$scope.getInvited = function(){
		
		$http.get('/laravel/public/getInvitedFriends').
		success(function(data, status, headers, config) {
			$scope.invited = data;
		});
	}

	$scope.acceptFriend = function(id){
		$http.post('/laravel/public/acceptInvitation', { 'id' : id }).
		success(function(data, status, headers, config) {
			$scope.init();
		});
	}
 
});

app.controller('profileController', function($scope, $http) {
 
	$scope.posts = [];
	$scope.loading = false;
	$scope.profileImageLoading = false;
	$scope.profileImage;
	$scope.showInfoForm = false;
	$scope.loadingInfo = false;
	$scope.infoOpen = false;

	//global vars info
	$scope.residence;
	$scope.work;
	$scope.bestfood;
	$scope.hobbys;
	$scope.email;
	$scope.school;
	$scope.age;
 
	$scope.init = function() {
		$scope.loading = true;
		$http.get('/laravel/public/getProfile/'+profileId).
		success(function(data, status, headers, config) {
			$scope.profilePosts = data.profilePosts;
			$scope.profileInfo = data.profileInfo[0];
			$scope.loading = false; 
		});
	}
	$scope.init();

	//----------------------------
	//Upload profile image ajaxform
	//----------------------------
	$('#uploadBtn').change(function(){
		$('#profile-image-upload').submit();
    });	

    $('#profile-image-upload').ajaxForm({ 
        dataType:  'json', 
        beforeSubmit:  startLoaderProfileImage,
        success:   processJsonProfileImage
	}); 

    function startLoaderProfileImage(){
    	console.log('start loading');
    	$scope.profileImageLoading = true;
    	$scope.$apply();
	}	

	function processJsonProfileImage(data){
	    if(data.success == false){
	    	$('#file-name').html('');
		    $scope.profileImageLoading = false;
		    $scope.$apply();
	    }
	    if(data.success == true){
	        $scope.updateProfileImage();
	    }
	}

	$scope.updateProfileImage = function(){
		$http.get('/laravel/public/profile/profileImage').
		success(function(data, status, headers, config) {
			$scope.profileImage = '/' + data;
			$('#file-name').html('');
			$scope.profileImageLoading = false;
		});	
	}

	//----------------------------
	//Edit profile info ajaxform
	//----------------------------
	 $('#profile-info-form').ajaxForm({ 
        dataType:  'json', 
        beforeSubmit:  startLoaderInfoEdit,
        success:   processJsonInfoEdit
	}); 

	function startLoaderInfoEdit(){
		$scope.loadingInfo = true;
    	$scope.$apply();
	}	

	function processJsonInfoEdit(data){
	    if(data.success == false){
		    console.log(data.message);
	    }
	    if(data.success == true){

	    	$scope.residence = data.infoArray.residence;
			$scope.work = data.infoArray.work;
			$scope.bestfood = data.infoArray.bestfood;
			$scope.hobbys = data.infoArray.hobbys;
			$scope.email = data.infoArray.email;
			$scope.school = data.infoArray.school;
			$scope.age = data.infoArray.age;
			//handle form layout
			$('.profile-info-content').show();
	        $('.profile-info-form').slideUp();
	        $('.profile-info-form .submit-info').hide();
	        $('.info-btn').show();
	        $scope.showInfoForm = false;
	        $scope.loadingInfo = false;

	        if($('.tour-startingTour').length > 0){
	        	window.startingTour.next();
	        }
	        
	        $scope.$apply();
	    }
	} 

	$scope.openAllInfo = function (){
		$scope.infoOpen = true;
		$(".profile-info-content").animate({height: $(".profile-info-content").get(0).scrollHeight}, 500 );
	}

	$scope.closeAllInfo = function (){
		$scope.infoOpen = false;
		$(".profile-info-content").animate({height: '150px'}, 500 );
	}
});


app.controller('getPhotosController', function($scope, $http) {
 
	$scope.loading = false;

	$scope.init = function(profileId) {
		$scope.loading = true;
		$http.get('/laravel/public/profile/user/'+profileId+'/getPhotos').
		success(function(data, status, headers, config) {
			$scope.photos = data;
			$scope.loading = false;
		});
	}
});

app.controller('chatController', function($scope, $http) {
 
	$scope.loading = false;
	$scope.messages = [];
	$scope.profile;
	$scope.receiver;
	$scope.friendId;

	//socket script
	var socket = io.connect('http://localhost:3000');
    var stopTypingVar;

    $('.list-group .list-group-item a').click(function(){
    	var text = $(this).html();
    	$('#myModal').modal('hide');
    	$('#m').val(text);

    	var message = { message: $('#m').val(), senderId: userId, receiverId : $scope.friendId };
		socket.emit('chat message', message);
		$('#m').val('');
		$scope.addMessage(message);

		return false;

    })

    $('form').submit(function(){

    	var message = { message: $('#m').val(), senderId: userId, receiverId : $scope.friendId };
		socket.emit('chat message', message);
		$('#m').val('');
		$scope.addMessage(message);

		return false;
    });

    $("#m").on("input", function() {
        socket.emit('is typing', true);

        window.clearTimeout(stopTypingVar);

        stopTypingVar = setTimeout(function(){ 
          socket.emit('is typing', false);
        }, 3000);
    });

    socket.on('chat message', function(msg){
    	if(userId == msg.receiverId || userId == msg.senderId){
			$scope.appendMessage(msg);
    	}
      
    });

    socket.on('is typing', function(typing){
	      if(typing == true){
	        $('.isTyping').html('aan het typen...');
	      }
	      if(typing == false){
	        $('.isTyping').html(' ');
	      }
    });

	$scope.init = function() {
		$scope.loading = true;
		$http.get('/laravel/public/getCurrentProfile').
		success(function(data, status, headers, config) {
			$scope.profile = data[0];
			$scope.loading = false;
		});
	}

	$scope.addMessage = function(message){
		$http.post('/laravel/public/addMessage', message).
		success(function(data, status, headers, config){
			console.log(data);
		})
	}

	$scope.getFriends = function() {
		$scope.loading = true;
		$http.get('/laravel/public/getFriends').
		success(function(data, status, headers, config) {
			$scope.friends = data;
			$scope.loading = false; 
		});
	}

	$scope.getMostRecentChat = function(){
		$('#messages .temp').remove();
		$http.get('/laravel/public/getMostRecentChat').
		success(function(data, status, headers, config) {
			if(data != 'empty'){
				$scope.messages = data.messages;
				$scope.receiver = data.receiverUser[0];
				$scope.friendId = data.receiverUser[0].id;
				$scope.loading = false; 

				setTimeout(function(){ 
					$('.panel-body').scrollTop($('.chat-container .panel-body .row').height() + 200); 
					$('.friend_' + $scope.friendId).addClass('active');
				}, 100);
				$('#sent-form').show();
				
			}else{
				$scope.empty = true;
			}
		});
	}

	$scope.getChat = function(profileId){
		$scope.friendId = profileId;
		$scope.empty = false;
		$scope.loading = true;
		$('.chat-friends').removeClass('active');
		$('.friend_' + $scope.friendId).addClass('active');
		$('#sent-form').show();
		$('#messages .temp').remove();
		$http.get('/laravel/public/getChat/' + $scope.friendId).
		success(function(data, status, headers, config) {
			$scope.messages = data.messages;
			$scope.receiver = data.receiverUser[0];
			$scope.loading = false; 
			setTimeout(function(){ $('.panel-body').scrollTop($('.chat-container .panel-body .row').height() + 200); }, 5);
		});
	}
	
	$scope.appendMessage = function(msg){
		if(msg.senderId == userId){
			$('#messages').append('<div class="clearfix temp row"><div class="message message-sent clearfix col-md-12 ng-scope"><span class="chat-image" style="background : url(../public/uploads/user_'+ $scope.profile.id +'/'+ $scope.profile.filename +') top center; background-size:cover;"></span><a href="http://localhost/laravel/public/profile/user/'+ $scope.profile.id +'"><span class="profile-name">'+ $scope.profile.name +'</span></a><div class="message-text"><p class="ng-binding">'+ msg.message+'</p></div></div></div>');
			console.log('sent by me');
		}else{
			$('#messages').append('<div class="clearfix temp row"><div class="message message-sent clearfix col-md-12 ng-scope"><span class="chat-image" style="background : url(../public/uploads/user_'+ $scope.receiver.id +'/'+ $scope.receiver.filename +') top center; background-size:cover;"></span><a href="http://localhost/laravel/public/profile/user/'+ $scope.receiver.id +'"><span class="profile-name">'+ $scope.receiver.name +'</span></a><div class="message-text"><p class="ng-binding">'+ msg.message+'</p></div></div></div>');
			console.log('sent by him');
		}
	}

	$scope.init();
	$scope.getMostRecentChat();
	$scope.getFriends();
});

function openInfoEdit(){
		$('.profile-info-content').hide();
		$('.info-btn').hide();
		$('.profile-info-form').slideDown();
		$('.profile-info-form .submit-info').show();
	}

